class ColsLexRejector extends Rejector
{
    @Override boolean reject(Grid g) {
        return !colsLexRecursive(g, 0, 0, g.width);
    }
    
    private boolean colsLexRecursive(Grid g, int r, int a, int b) {
        if(r > g.currentRowIndex) { return true; }
        int t = g.rows[r].asNumber;
        t >>= g.width - b;
        t &= (1 << (b - a)) - 1;
        int m = Integer.bitCount(t);
        if(t != ((1 << m) - 1) << (b-a-m)) {
            return false;
        } else if(!colsLexRecursive(g, r+1, a, a+m)) {
            return false;
        } else {
            return colsLexRecursive(g, r+1, a+m, b);
        }
    }
}
