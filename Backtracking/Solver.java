import java.util.List;
import java.util.ArrayList;

public class Solver {
    public abstract static class Factory
    {
        Navigator navigator;
        List<Rejector> rejectors;
        
        Factory() {
            rejectors = new ArrayList<Rejector>();
            rejectors.add(new Rejector.Rectangle());
        }
        
        public Solver getSolver(int width, int height) {
            return new Solver(width, height, navigator, rejectors);
        }
        public int solve(int width, int height) {
            return getSolver(width, height).solve();
        }
    }
    
    Grid grid;
    int width, height, bestWeight=0;
    long nodesVisited;
    
    Navigator navigator;
    List<Rejector> rejectors;
    
    public Solver(int newWidth, int newHeight, Navigator newNavigator, List<Rejector> newRejectors) {
        width = newWidth;
        height = newHeight;
        navigator = newNavigator;
        rejectors = newRejectors;
    }
    
    public void setLowerBound(int lowerBound) {
        bestWeight = Math.max(bestWeight, lowerBound);
    }
    
    /*
     * we could reject a node if
     * - there's no valid completion (i.e. it contains a rectangle),
     * - there's no completion better than what we already found, or
     * - it's symmetric to a node we already tried (or will try later).
     */
    boolean rejectNode() {
        if(grid.currentRow() == null) { return false; }
        for(Rejector r : rejectors) {
            if(r.reject(this)) { return true; }
        }
        return false;
    }
    
    // convenience methods for accessing the navigator
    void ascend() { navigator.ascend(grid); }
    void descend() { navigator.descend(grid); }
    boolean canAdvance() { return navigator.canAdvance(grid); }
    void advance() { navigator.advance(grid); }
    
    // solve using a backtracking algorithm
    public int solve() {
        grid = new Grid(width, height);
        // count how many nodes we visit
        nodesVisited = 0;
        // this method does all of the work.
        visitCurrentNode();
        // after visiting the root node, we're done!
        return bestWeight;
    }
    
    // visit one node in the search tree
    private void visitCurrentNode() {
        // we are visiting another node, so increment this counter
        nodesVisited++;
        
        if(rejectNode()) { return; }
        
        if(grid.isComplete()) {
            // we reached the bottom of the search tree, so the grid is complete.
            int newWeight = grid.weight();
            // check if this solution is better than what we had before
            if(newWeight > bestWeight) {
                bestWeight = newWeight;
                // uncomment this line to see the solutions found.
                //System.out.println(grid);
            }
            return;
        }
        
        // move down the tree to visit this node's children
        descend();
        
        // visit every child node
        while(true) {
            // visit this child node
            visitCurrentNode();
            // stop if there are no more siblings to visit
            if(!canAdvance()) { break; }
            // otherwise move on to the next sibling
            advance();
        }
        
        // after visiting all of this node's children, backtrack
        ascend();
    }
}
