public interface Navigator {
    public void descend(Grid g);
    public boolean canAdvance(Grid g);
    public void advance(Grid g);
    public void ascend(Grid g);
}
