class BT extends Solver.Factory {
    BT() { this(true); }
    BT(boolean decrement) {
        super();
        if(decrement) {
            navigator = new DecrementNavigator();
        } else {
            navigator = new IncrementNavigator();
        }
        rejectors.add(new ConvexOptimisationBound());
        rejectors.add(new ColsLexRejector());
    }
}
