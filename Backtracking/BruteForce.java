class BruteForce extends Solver.Factory {
    BruteForce() {
        super();
        navigator = new NaiveBT.NaiveNavigator();
        rejectors.clear();
        rejectors.add(new FullGridRejector());
    }
    
    static class FullGridRejector extends Rejector {
        @Override boolean reject(Grid g) {
            // only reject completed grids; this guarantees exploring the entire search tree.
            if(!g.isComplete()) { return false; }
            // need to check every pair of rows for rectangles
            for(int i = 1; i < g.height; i++) {
                for(int j = 0; j < i; j++) {
                    if(g.rows[i].hasRectangleWith(g.rows[j])) { return true; }
                }
            }
            return false;
        }
    }
}




