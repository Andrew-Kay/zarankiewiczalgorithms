class ConvexOptimisationBound extends Rejector.Bound {
    int upperBound(Grid g) {
        /* 
         * we use n*(n-1) instead of n*(n-1)/2 for counting pairs, which saves divisions
         * but gives the same answer.
         */
        int unclaimedPairs = g.width*(g.width-1);
        for(int i = 0; i <= g.currentRowIndex; i++) {
            int w = g.rows[i].weight();
            unclaimedPairs -= w*(w-1);
        }
        int remainingRows = g.height - g.currentRowIndex - 1;
        assert remainingRows > 0;
        int wStar = 1;
        while(remainingRows*(wStar+1)*wStar <= unclaimedPairs) { wStar++; }
        int wStarPairs = wStar*(wStar-1);
        int wStarPlusOnePairs = (wStar+1)*wStar;
        int L = (unclaimedPairs - remainingRows*wStarPairs)/(wStarPlusOnePairs - wStarPairs);
        return g.weight() + remainingRows*wStar + L;
    }
}
