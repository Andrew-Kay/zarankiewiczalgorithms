class Row {
    int width, mask, asNumber;
    
    Row(int width, int asNumber) {
        this.width = width;
        this.asNumber = asNumber;
        this.mask = (1 << width) - 1;
    }
    Row(Row r) {
        this(r.width, r.asNumber);
    }
    
    // returns the weight, i.e. the number of 1s in this row
    int weight() { return Integer.bitCount(asNumber); }
    
    // determines whether any rectangle occurs across these two rows
    boolean hasRectangleWith(Row other) {
        int t = this.asNumber & other.asNumber;
        return (t&(t-1)) != 0;
    }
    
    @Override public String toString() {
        return String.format("%" + width + "s", Integer.toBinaryString(asNumber)).replace(" ", "0");
    }
}
