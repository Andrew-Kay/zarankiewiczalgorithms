class Test {
    static void efficiency(Solver.Factory f, int maxN) {
        int z = 0;
        long v = 0;
        System.out.println(f);
        System.out.println("m,n,z,time,nodes");
        for(int n = 1; n <= maxN; n++) {
            long startTime = System.nanoTime();
            Solver s = f.getSolver(n,n);
            z = s.solve();
            v = s.nodesVisited;
            double time = (System.nanoTime() - startTime)/1_000_000_000.0;
            System.out.println(n + "," + n + "," + z + "," + time + "," + v);
        }
    }
    
    static void testSolve(Solver.Factory f, int maxN) {
        int z = 0;
        long v = 0;
        for(int n = 1; n <= maxN; n++) {
            System.out.println("STARTING: n = " + n + "\n");
            Solver s = f.getSolver(n,n);
            long startTime = System.nanoTime();
            z = s.solve();
            double time = (System.nanoTime() - startTime)/1_000_000_000.0;
            v = s.nodesVisited;
            System.out.println("BEST: n = " + n + ", z = " + z + ", t = " + time + ", v = " + v + "\n\n");
        }
    }
}
