abstract class Rejector {
    abstract boolean reject(Grid g);
    
    boolean reject(Solver a) {
        return reject(a.grid);
    }
    
    static class Rectangle extends Rejector
    {
        @Override boolean reject(Grid g) {
            /*
             * simplest case: just check if the grid is invalid. we only need to check
             * the current row for rectangles, because higher rows were checked already.
             */
            return g.currentRowHasRectangle();
        }
    }
    
    static abstract class Bound extends Rejector {
        @Override boolean reject(Solver a) {
            if(a.grid.isComplete()) {
                return false;
            } else {
                return upperBound(a.grid) < a.bestWeight;
            }
        }
        
        @Override boolean reject(Grid g) {
            throw new UnsupportedOperationException();
        }
        
        abstract int upperBound(Grid g);
    }
    
    static class ConstantBound extends Bound {
        int b;
        ConstantBound(int upperBound) { b = upperBound; }
        @Override int upperBound(Grid g) { return b; }
    }
}
