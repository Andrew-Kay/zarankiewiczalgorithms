class BTWeightLex extends Solver.Factory {
    BTWeightLex() {
        super();
        navigator = new WeightLexNavigator();
        rejectors.add(new DescendingRowWeightBound());
    }
    
    static class WeightLexNavigator extends DecrementNavigator {
        @Override public void advance(Grid g) {
            Row r = g.currentRow();
            int t = r.asNumber;
            int ones = Integer.numberOfTrailingZeros(~t);
            t &= (t+1);
            if(t != 0) {
                int bit = Integer.lowestOneBit(t);
                t ^= bit;
                int mask = bit-1;
                t ^= mask;
                mask >>= (ones+1);
                t ^= mask;
            } else {
                t = r.mask ^ (r.mask >> (ones-1));
            }
            r.asNumber = t;
        }
    }
    
    static class DescendingRowWeightBound extends Rejector.Bound {
        @Override int upperBound(Grid g) {
            return g.weight() + (g.height - 1 - g.currentRowIndex)*g.currentRow().weight();
        }
    }
}
