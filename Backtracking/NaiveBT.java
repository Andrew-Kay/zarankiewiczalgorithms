class NaiveBT extends Solver.Factory {
    NaiveBT() {
        navigator = new NaiveNavigator();
        //rejectors.add(new NaiveBound());
    }
    
    static class NaiveNavigator extends IncrementNavigator {
        @Override public void descend(Grid g) { g.pushRow(new Row(g.width, 0)); }
    }
    
    static class NaiveBound extends Rejector.Bound {
        @Override int upperBound(Grid g) {
            return g.weight() + (g.height - g.currentRowIndex)*g.width;
        }
    }
}
