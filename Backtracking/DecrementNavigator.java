public class DecrementNavigator implements Navigator {
    public void descend(Grid g) {
        Row r = g.currentRow();
        if(r != null) {
            g.pushRow(new Row(r));
        } else {
            g.pushRow(new Row(g.width, (1 << g.width) - 1));
        }
    }
    public void advance(Grid g) { g.currentRow().asNumber--; }
    public boolean canAdvance(Grid g) { return g.currentRow().asNumber > 0; }
    public void ascend(Grid g) { g.popRow(); }
}
