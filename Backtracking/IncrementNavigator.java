public class IncrementNavigator implements Navigator {
    public void descend(Grid g) {
        Row r = g.currentRow();
        if(r != null) {
            g.pushRow(new Row(r));
        } else {
            g.pushRow(new Row(g.width, 0));
        }
    }
    public boolean canAdvance(Grid g) {
        Row r = g.currentRow();
        return r.asNumber < r.mask;
    }
    public void advance(Grid g) { g.currentRow().asNumber++; }
    public void ascend(Grid g) { g.popRow(); }
}
