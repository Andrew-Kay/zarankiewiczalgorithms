class Grid {
    int width, height;
    Row[] rows;
    /*
     * currentRowIndex is the index of the row we're currently operating on.
     * -1 indicates that we haven't yet inserted row 0.
     */
    int currentRowIndex=-1;
    
    Grid(int width, int height) {
        this.width = width;
        this.height = height;
        this.rows = new Row[height];
    }
    
    // returns the total number of 1s in this grid
    int weight() {
        int w = 0;
        for(int i = 0; i <= currentRowIndex; i++) {
            w += rows[i].weight();
        }
        return w;
    }
    
    // behave like a stack of rows
    Row currentRow() {
        if(currentRowIndex >= 0) {
            return rows[currentRowIndex];
        } else {
            return null;
        }
    }
    void pushRow(Row r) {
        currentRowIndex++;
        rows[currentRowIndex] = r;
    }
    void popRow() {
        rows[currentRowIndex] = null;
        currentRowIndex--;
    }
    
    // determines whether the grid is complete, by checking whether the bottom row has been inserted
    boolean isComplete() { return currentRowIndex == height-1; }
    
    // determines whether the most recently-added row created a rectangle
    boolean currentRowHasRectangle() {
        Row r = currentRow();
        if(r == null) { return false; }
        for(int i = 0; i < currentRowIndex; i++) {
            if(rows[i].hasRectangleWith(r)) { return true; }
        }
        return false;
    }
    
    @Override public String toString() {
        StringBuilder output = new StringBuilder();
        output.append("Grid: " + width + " by " + height + ", weight " + weight() + "\n");
        for(Row r : rows) {
            if(r == null) {
                output.append("...\n");
                break;
            }
            output.append(r);
            output.append("\n");
        }
        return output.toString();
    }
    
}