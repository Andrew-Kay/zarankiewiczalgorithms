This repository stores a collection of algorithms for the Zarankiewicz problem, written by Andrew Kay, to support a series of blog posts.

For more details, see
http://andrewkay.name/blog/post/the-zarankiewicz-rollercoaster/
