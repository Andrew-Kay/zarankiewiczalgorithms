"""
Exhaustive proof that row and column symmetries do not guarantee
that a grid has a symmetric counterpart whose rows are in both lex
order and descending weight order.
"""

G = [
	[1,1,1,1,0,0,0,0,],
	[1,0,0,0,1,1,1,0,],
	[1,0,0,0,0,0,0,1,],
	[0,1,0,0,1,0,0,1,],
	[0,0,1,0,0,1,0,1,],
	[0,0,0,1,0,0,1,1,],
]

m,n = len(G), len(G[0])

def permute_rows(M, row_p):
	return [ M[row_p[i]] for i in range(m) ]
def permute_cols(M, col_p):
	return [ [ R[col_p[j]] for j in range(n) ] for R in M ]
#

def rows_in_lex_order(M):
	# our lex order is equivalent to > on lists
	return all( M[i] >= M[i+1] for i in range(m-1) )
def rows_in_descending_weight_order(M):
	weights = [ sum(R) for R in M ]
	return all( weights[i] >= weights[i+1] for i in range(m-1) )
#

from itertools import permutations
row_permutations = list(permutations(range(m)))
col_permutations = list(permutations(range(n)))

if __name__ == '__main__':
	found = False
	for row_p in row_permutations:
		G_r = permute_rows(G, row_p)
		if not rows_in_descending_weight_order(G_r):
			continue
		for col_p in col_permutations:
			G_c = permute_cols(G_r, col_p)
			if not rows_in_lex_order(G_c):
				continue
			print('Found:\n', '\n'.join( str(R) for R in G_c ))
			found = True
	if not found:
		print('Found nothing.')
# prints 'Found nothing.'
